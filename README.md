Little Brother Database (lbdb)
==============================

The Little Brother Database (lbdb) is a simple Python implementation of a [Time-Series Database](https://en.wikipedia.org/wiki/Time_series_database) meant for education, artistic experimentation and small-scale deployments (e.g. art installations).
