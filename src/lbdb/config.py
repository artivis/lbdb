
import yaml

DEFAULT_CONFIG = {
    'host': 'localhost',
    'port': 8080,
    'store': {
        'type': 'mem'
    },
    'keys': []
}

class ServerConfig(object):
    """Load and maintain server configuration variables"""

    def __init__(self, config_file):
        config_data = DEFAULT_CONFIG
        with open(config_file, 'r') as config:
            config_data.update (yaml.load(config))
        self.host = config_data['host']
        self.port = config_data['port']
        self.store = config_data['store']
        self.keys = config_data['keys']
