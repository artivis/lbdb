#! /usr/bin/env python3

# TODO:
#  - flesh out handlers using memstore
#     - view/update event
#  - use (py)rocksdb for storage (rockstore)
#  - tests, we need tests
#  - validate JSON requests with jsonschema
#  - when the time comes for complex time series operations:
#    - use pypond for data structures and prebuilt ops
#    - use qpack for storage format
#  - add command line options to point to config file and/or override config
#  - better inline docs
#  - publish on gitlab
#  - post 1.0
#    - force HTTPS
#    - make sure concurrent requests are working properly 
#    - use a proper ts engine backend (like beringel)

import sys
import asyncio

from aiohttp import web
import aiohttp_cors

from config import ServerConfig
from store import create_store

#
# middleware
#

@web.middleware
async def verify_api_key (request, handler):
    if (request.method != 'OPTIONS'):
        key = request.headers.get('X-API-Key', None)
        if not key or key not in request.app['config'].keys:
            return web.json_response (status=403,
                                      data={ 'error': "API key missing or incorrect" });
    response = await handler(request)
    return response

#
# resource handlers
#

async def handle_create_series (request):
    data = await request.json()
    ns = request.headers['X-Api-Key']
    store = request.app['store']
    if 'series' in data:
        if store.contains (ns, data['series']):
            return web.json_response (status=409,
                                      data = { 'error': "Resource already exists "})
        else:
            store.create_series (ns, data['series'])
            return web.Response (status=201, headers={'Location': f"/api/series/{data['series']}"})
    else:
        return web.json_response (status=400,
                                  data={ 'error': "Missing series name parameter" })

async def handle_list_series (request):
    store = request.app['store']
    ns = request.headers['X-Api-Key']
    series = store.list_series(ns)
    return web.json_response (status=200, data={'series': series})
    
async def handle_delete_series (request):
    store = request.app['store']
    ns = request.headers['X-Api-Key']
    sname = request.match_info['sid']
    if not store.contains(ns, sname):
        return web.json_response(status=404, data={'error':"Series not found"})
    elif store.delete_series (ns, sname):
        return web.Response(status=204)
    else:
        return web.json_response(status=500, data={'error':"Not possible to delete series"})

async def handle_query_series(request):
    sname = request.match_info['sid']
    ns = request.headers['X-Api-Key']
    store = request.app['store']
    if not store.contains (ns, sname):
        return web.json_response (status = 404, data = { 'error': "Series not found" })
    ts_from = request.query.get('from', None)
    ts_to = request.query.get('to', None)
    op = request.query.get('op', 'query')

    rdata = {}
    if op == 'count':
        rdata['count'] = store.count_events (ns, sname, ts_from, ts_to)
    elif op == 'delete':
        if store.delete_events (ns, sname, ts_from, ts_to):
            return web.Response (status=204)
        else:
            return web.json_response(status=500)
    else:
        rdata['events'] = [ { 'timestamp': ts, 'data': dt } for
                            ts,dt in store.query_events (ns, sname, ts_from, ts_to) ]
    return web.json_response (status=200, data=rdata)
    
async def handle_create_event(request):
    sname = request.match_info['sid']
    ns = request.headers['X-Api-Key']
    store = request.app['store']
    data = await request.json();
    # TODO: validate request 
    if store.add_event (ns, sname, data['timestamp'], data['data']):
        return web.Response (status=201)
    else:
        return web.Response (status=500)

async def handle_query_event(request):
    print ('handle_query_event():', request.match_info['sid'], 'ts: ', request.match_info['timestamp'])
    return web.Response(status=200)

async def handle_update_event(request):
    print ('handle_update_event():', request.match_info['sid'], 'ts: ', request.match_info['timestamp'])
    return web.Response(status=200)

#
# Main loop
#

def main():

    # start the web frontend
    app = web.Application (middlewares=[verify_api_key])

    # load config and setup store
    app['config'] = ServerConfig ('config.yaml')
    app['store'] = create_store (app['config'].store)
    
    # create CORS config
    cors = aiohttp_cors.setup(app, defaults={
        '*': aiohttp_cors.ResourceOptions(
            expose_headers="*",
            allow_headers=['Content-Type', 'X-API-Key'],
            max_age=3600
        )
    })

    # setup resources and add them to CORS config
    series_collection = cors.add (app.router.add_resource('/api/series'))
    cors.add (series_collection.add_route('POST', handle_create_series))
    cors.add (series_collection.add_route('GET', handle_list_series))

    series = cors.add (app.router.add_resource ('/api/series/{sid}'))
    cors.add (series.add_route('GET', handle_query_series))
    cors.add (series.add_route('POST', handle_create_event))
    cors.add (series.add_route('DELETE', handle_delete_series))

    event = cors.add (app.router.add_resource ('/api/series/{sid}/{timestamp}'))
    cors.add (event.add_route ('GET', handle_query_event))
    cors.add (event.add_route ('PUT', handle_update_event))

    # run app
    web.run_app (app, host=app['config'].host, port=app['config'].port)

if __name__ == '__main__':
    main()
