

class TimeSeriesStore(object):
    """Base class for Time-Series stores"""

    def __init__(self, config):
        pass

    def contains(self, ns, sname):
        pass
    
    def create_series (self, ns, sname):
        pass

    def list_series (self, ns):
        pass

    def delete_series (self, ns, sname):
        pass

    def add_event (self, ns, sname, timestamp, data):
        pass

    def get_event (self, ns, sname, timestamp):
        pass
    
    def query_events (self, ns, sname, start=None, end=None):
        pass

    def count_events (self, ns, sname, start=None, end=None):
        pass

    def delete_events (self, ns, sname, start=None, end=None):
        pass


class MemStore(TimeSeriesStore):
    """Simple (and volatile) memory-based time-series store"""

    def __init__(self):
        self.series = {}

    def _make_key(self, ns, sname):
        return ns + '-' + sname
        
    def contains(self, ns, sname):
        series = self._make_key (ns, sname)
        return (series in self.series)

    def _find_index (self, series, ts):
        if ts == 0 or ts == None:
            return ts
        timeline = self.series[series]
        index = 0
        while index < len(timeline) and timeline[index][0] < ts:
            index += 1
        return index

    def _find_range(self, series, start, end):
        return (self._find_index(series, start), self._find_index(series, end))
    
    def create_series (self, ns, sname):
        series = self._make_key (ns, sname)
        if series not in self.series:
            self.series[series] = []
            return True
        return False

    def list_series (self, ns):
        return [ s for s in self.series.keys() if s.startswith(ns) ]
    
    def delete_series (self, ns, sname):
        series = self._make_key(ns, sname)
        if series in self.series:
            del self.series[series]
            return True
        return False
            
    def add_event (self, ns, sname, timestamp, data):
        series = self._make_key (ns, sname)
        if series not in self.series:
            return False
        timeline = self.series[series]
        if len(timeline) == 0:
            timeline.append ((timestamp, data))
        elif timestamp > timeline[-1][0]:
            timeline.append ((timestamp, data))
        else:
            index = 0
            while timestamp > timeline[index][0]:
                index += 1
            timeline.insert (index, (timestamp, data))
        return True
            
    def get_event (self, series, timestamp):
        pass
    
    def query_events (self, ns, sname, start=None, end=None):
        series = self._make_key (ns, sname)
        timeline = self.series[series]
        ts_start, ts_end = self._find_range(series, start, end)
        return timeline[ts_start:ts_end]

    def count_events (self, ns, sname, start=None, end=None):
        return len(self.query_events(ns, sname, start, end))
        
    def delete_events (self, ns, sname, start=None, end=None):
        series = self._make_key (ns, sname)
        timeline = self.series[series]
        ts_start, ts_end = self._find_range(series, start, end)
        timeline[ts_start:ts_end] = []
        return True

def create_store(config):
    """ Factory method for Time-Series stores"""
    
    if config['type'] == 'mem':
        return MemStore()
    else:
        raise NotImplementedError()
